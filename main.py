import os
import pathlib
import re

import ftplib

from datetime import date, datetime
from getpass import getpass
from socket import gaierror
from time import sleep

from dotenv import load_dotenv

		
def downloadBackupFiles(ftp):
	'''Get the directory on the FTP server and download it to the destination
	   folder on the user's computer'''

	# FTPPrint a list of directories and files
	ftp.retrlines('LIST')

def FTPPrint(s):
	'''Wrap each FTPPrint with a timestamp'''
	print(getTime() + ': ' + s)

def FTPConnect():
	'''Connect to the server using FTP for downloading files'''

	# Create an FTP instance
	ftp = ftplib.FTP()

	# Connect to the server with given params
	connected = False
	while connected == False:
		try:
			ftp.connect(
				host=os.getenv('FDB_FTP_HOST'),
				port=int(os.getenv('FDB_FTP_PORT'))
			)
			connected = True
		except gaierror:
			print()
			FTPPrint('Host name could not be resolved (possible network issue)')
			FTPPrint('Trying again in 30 seconds..')
			sleep(30)
		except TimeoutError:
			print()
			FTPPrint('Host did not respond (possible network issue)')
			FTPPrint('Trying again in 30 seconds..')
			sleep(30)

	print()
	FTPPrint(
		'Connected to '
		+ os.getenv('FDB_FTP_HOST')
		+ ' port '
		+ str(os.getenv('FDB_FTP_PORT'))
		)
	print()

	# Login to the server with given credentials
	prompt = f'{getTime()}: Please enter the password for '
	prompt += f'{os.getenv("FDB_FTP_USERNAME")}: '
	logged_in = False
	while logged_in == False:
		try:
			ftp.login(
				user=os.getenv('FDB_FTP_USERNAME'),
				passwd=getpass(prompt=prompt)
			)
			FTPPrint('Logged in as ' + os.getenv('FDB_FTP_USERNAME'))
			print()
			logged_in = True
		except ftplib.error_perm:
			FTPPrint('Invalid password. Please try again.')
			print()
		except TimeoutError:
			print()
			FTPPrint('Host did not respond (possible network issue)')
			FTPPrint('Try again..')

	return ftp

def getDate():
	return date.today().strftime('%d %B %Y')

def getTime():
	return datetime.now().strftime('%H:%M')

def setupLocalFolders():
	'''Create the necessary folders for downloading and storing the backup
	   contents'''
	# Get the user's home directory
	home_dir = pathlib.Path.home()

	# Determine the parent backup folder location
	if os.getenv('FDB_BACKUP_FOLDER_LOCATION').lower() == 'home':
		backup_parent_dir_location = os.path.join(
			home_dir, 
			os.getenv('FDB_BACKUP_FOLDER_NAME')
		)
	else:
		backup_parent_dir_location = os.path.join(
			home_dir,
			'Desktop',
			os.getenv('FDB_BACKUP_FOLDER_NAME')
		)

	# Create the parent backup folder if it doesn't exist already
	try:
		os.mkdir(backup_parent_dir_location)
		FTPPrint(f'Created parent backup folder at {backup_parent_dir_location}')
	except FileExistsError:
		FTPPrint(f'Parent backup folder {backup_parent_dir_location} already exists')

	# Determine the current backup folder name
	time = getTime().replace(':', '.')
	backup_folder = f'''{os.getenv('FDB_BACKUP_FOLDER_NAME')} - {getDate()} {time}'''

	backup_dir_location = os.path.join(
		backup_parent_dir_location,
		backup_folder
	)

	# Create the backup folder if it doesn't exist already
	# Otherwise, rename it
	try:
		os.mkdir(backup_dir_location)
		FTPPrint(f'Created backup folder at {backup_dir_location}')
	except FileExistsError:
		FTPPrint(f'Backup folder {backup_dir_location} already exists')
		existing_file_pattern = re.compile(r'\[\d{1,2}\]')

		# This will add a counter to the end of the name of the folder
		counter = 1
		while os.path.exists(backup_dir_location):
			pattern = re.search(existing_file_pattern, backup_dir_location)
			if pattern == None:
				backup_dir_location += ' [1]'
			else:
				i1, i2 = pattern.span()[0], pattern.span()[1]
				backup_dir_location = backup_dir_location[0:i1] + f'[{str(counter)}]'
				counter += 1

		os.mkdir(backup_dir_location)
		FTPPrint(f'Created backup folder named {backup_dir_location}')

def main():
	os.system('cls')
	FTPPrint(getDate() + ': FTP Directory Backup program started')

	# Get the info needed for logging in and storing backups
	load_dotenv()

	# Connect to the FTP server
	ftp = FTPConnect()
	setupLocalFolders()
	downloadBackupFiles(ftp)

	# Gracefully close the connection
	ftp.quit()

if __name__ == '__main__':
	main()